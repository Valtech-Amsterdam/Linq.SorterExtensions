﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NUnit.Framework;
using Valtech.Amsterdam.Labs.Linq.SorterExtensions.Tests.Models;

namespace Valtech.Amsterdam.Labs.Linq.SorterExtensions.Tests
{
    [TestFixture] public class SortingTests
    {
        private const string TinyAlphabet = "ABCD";

        [Test] public void SortByAlphabetWithMethod()
        {
            // Assign
            var a = new AlphabetSorted { SortIndex = 'A' };
            var b = new AlphabetSorted { SortIndex = 'B' };
            var c = new AlphabetSorted { SortIndex = 'C' };
            var d = new AlphabetSorted { SortIndex = 'D' };
            var unsorted = new[] { b, c, d, a };
            var expected = new[] { a, b, c, d };

            // Act
            var sorted = unsorted
                .SortBy(AlphabetOrderingMethod)?
                .ToArray();

            // Assert
            Assert.That(expected, Is.EqualTo(sorted));
        }

        [Test] public void SortByAlphabetWithFunction()
        {
            // Assign
            var a = new AlphabetSorted { SortIndex = 'A' };
            var b = new AlphabetSorted { SortIndex = 'B' };
            var c = new AlphabetSorted { SortIndex = 'C' };
            var d = new AlphabetSorted { SortIndex = 'D' };
            var unsorted = new[] { b, c, d, a };
            var expected = new[] { a, b, c, d };

            // Act
            var sorted = unsorted
                // Does absolutely nothing tho
                .SortBy((first, second) => {
                    var sortIndexFirst = TinyAlphabet.IndexOf(first.SortIndex);
                    var sortIndexSecond = TinyAlphabet.IndexOf(second.SortIndex);

                    if (sortIndexFirst > sortIndexSecond) return 1;
                    if (sortIndexFirst < sortIndexSecond) return -1;
                    return 0;
                })?
                .ToArray();

            // Assert
            Assert.That(expected, Is.EqualTo(sorted));
        }

        [Test] public void SortByAlphabetWithExpression()
        {
            // Assign
            var a = new AlphabetSorted { SortIndex = 'A' };
            var b = new AlphabetSorted { SortIndex = 'B' };
            var c = new AlphabetSorted { SortIndex = 'C' };
            var d = new AlphabetSorted { SortIndex = 'D' };
            var unsorted = new[] { b, c, d, a };
            var expected = new[] { a, b, c, d };

            // Act
            var sorted = unsorted
                .SortBy((first, second) => AlphabetOrderingMethod(first, second), false)?
                .ToArray();

            // Assert
            Assert.That(expected, Is.EqualTo(sorted));
        }

        [ExcludeFromCodeCoverage]
        private static int AlphabetOrderingMethod(AlphabetSorted first, AlphabetSorted second)
        {
            Assert.NotNull(first); Assert.NotNull(second);

            var sortIndexFirst = TinyAlphabet.IndexOf(first.SortIndex);
            var sortIndexSecond = TinyAlphabet.IndexOf(second.SortIndex);

            if (sortIndexFirst > sortIndexSecond) return 1;
            if (sortIndexFirst < sortIndexSecond) return -1;
            return 0;
        }
    }
}