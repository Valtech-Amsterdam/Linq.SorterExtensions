﻿using System.Diagnostics.CodeAnalysis;

namespace Valtech.Amsterdam.Labs.Linq.SorterExtensions.Tests.Models
{
    [ExcludeFromCodeCoverage]
    public class AlphabetSorted
    {
        public char SortIndex { get; set; }
    }
}
