﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Valtech.Amsterdam.Labs.DelegateComparer;

namespace Valtech.Amsterdam.Labs.Linq.SorterExtensions
{
    /// <summary>
    /// Extensions for sorting <see cref="IEnumerable{T}"/>s
    /// </summary>
    public static class SorterExtensions
    {
        /// <summary>
        /// Use an Expression to custom sort the object
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="compareExpression">Expression used to compare enumerables</param>
        /// <param name="cached">Whether or not to cache the compiled Expression (defaults to true)</param>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> SortBy<TModel>(this IEnumerable<TModel> enumerable, 
            Expression<Func<TModel, TModel, int>> compareExpression, bool cached = true)
            => enumerable.OrderBy(x => x, cached
                ? (IComparer<TModel>)new CachedDelegateComparer<TModel>(compareExpression)
                : (IComparer<TModel>)new DelegateComparer<TModel>(compareExpression));
        /// <summary>
        /// Use a Method to custom sort the object
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="compareFunction">Method used to compare enumerables</param>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> SortBy<TModel>(this IEnumerable<TModel> enumerable, 
            Func<TModel, TModel, int> compareFunction)
            => enumerable.OrderBy(x => x, 
                new DelegateComparer<TModel>((first, second) => compareFunction(first, second)));

        /// <summary>
        /// Use an Expression to custom sort the object
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="compareExpression">Expression used to compare enumerables</param>
        /// <param name="cached">Whether or not to cache the compiled Expression (defaults to true)</param>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> SortByDescending<TModel>(this IEnumerable<TModel> enumerable, 
            Expression<Func<TModel, TModel, int>> compareExpression, bool cached = true)
            => enumerable.OrderByDescending(x => x, cached
                ? (IComparer<TModel>)new CachedDelegateComparer<TModel>(compareExpression)
                : (IComparer<TModel>)new DelegateComparer<TModel>(compareExpression));
        /// <summary>
        /// Use a Method to custom sort the object
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="compareFunction">Method used to compare enumerables</param>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> SortByDescending<TModel>(this IEnumerable<TModel> enumerable, 
            Func<TModel, TModel, int> compareFunction, bool cached = true)
            => enumerable.OrderByDescending(x => x, 
                new DelegateComparer<TModel>((first, second) => compareFunction(first, second)));

        /// <summary>
        /// Use an Expression to custom sort the object
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="compareExpression">Expression used to compare enumerables</param>
        /// <param name="cached">Whether or not to cache the compiled Expression (defaults to true)</param>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> ThenSortBy<TModel>(this IOrderedEnumerable<TModel> enumerable,
            Expression<Func<TModel, TModel, int>> compareExpression, bool cached = true)
            => enumerable.ThenBy(x => x, cached
                ? (IComparer<TModel>)new CachedDelegateComparer<TModel>(compareExpression)
                : (IComparer<TModel>)new DelegateComparer<TModel>(compareExpression));
        /// <summary>
        /// Use a Method to custom sort the object
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="compareFunction">Method used to compare enumerables</param>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> ThenSortBy<TModel>(this IOrderedEnumerable<TModel> enumerable,
            Func<TModel, TModel, int> compareFunction)
            => enumerable.ThenBy(x => x, 
                new DelegateComparer<TModel>((first, second) => compareFunction(first, second)));

        /// <summary>
        /// Use an Expression to custom sort the object
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="compareExpression">Expression used to compare enumerables</param>
        /// <param name="cached">Whether or not to cache the compiled Expression (defaults to true)</param>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> ThenSortByDescending<TModel>(this IOrderedEnumerable<TModel> enumerable,
            Expression<Func<TModel, TModel, int>> compareExpression, bool cached = true)
            => enumerable.ThenByDescending(x => x, cached
                ? (IComparer<TModel>)new CachedDelegateComparer<TModel>(compareExpression)
                : (IComparer<TModel>)new DelegateComparer<TModel>(compareExpression));
        /// <summary>
        /// Use a Method to custom sort the object
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="compareFunction">Method used to compare enumerables</param>
        /// <returns></returns>
        public static IOrderedEnumerable<TModel> ThenSortByDescending<TModel>(this IOrderedEnumerable<TModel> enumerable,
            Func<TModel, TModel, int> compareFunction)
            => enumerable.ThenByDescending(x => x, 
                new DelegateComparer<TModel>((first, second) => compareFunction(first, second)));
    }
}
