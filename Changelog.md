# Changelog

## Release 1.1.0 (latest)
* Added opt-out for caching

## Release 1.0.0 Initial Release
* Initial publication