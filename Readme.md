[//]: # (Header)

<div align="center">
  <a href="https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions">
    <img height="250" width="250" src="https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/raw/master/resource/Logo.png" alt="Linq.SorterExtensions">
  </a>
</div>

[![Downloads][downloads-image]][nuget-url] 
[![Build Status][build-image]][build-url] [![Coveralls Status][coveralls-image]][coveralls-url]

[//]: # (Documentation)

## What is Linq.SorterExtensions  
This is a library providing you with a means to use the OrderBy method from Linq but use a Method or Expression  
in the class responsible for sorting instead of the instance that's being sorted.  
  
## Why do I need this?  
Honestly I hope you never have to implement custom sorting logic inside of your linq chain ever. However, sometimes you have to.  
This library is usefull if you have custom sorting logic depending on parents and you need to sort on a name property after that.  
Or if you have to custom sort an object you don't have controll over, for example a library or a framework like a CMS.
  
## Installing the library
Simply check out the [NuGet Package][nuget-url] using:  
```Batchfile
Install-Package Valtech.Amsterdam.Labs.Linq.SorterExtensions 
```
  
## Using the library  
Simply call:  
```csharp
	{enumerable}.SortBy({OrderingMethod});
```  
Or:
```csharp
	{enumerable}.SortBy((first, second) => {expression});
```  
See an example here: [src/Valtech.Amsterdam.Labs.Linq.SorterExtensions.Tests/SortingTests.cs](https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/blob/master/src/Valtech.Amsterdam.Labs.Linq.SorterExtensions.Tests/SortingTests.cs)  
  
## [Contributing][contributing-url]  
[contributing-url]: https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/blob/master/Contributing.md
See the [Contribution guide][contributing-url] for help about contributing to this project.
  
## [Changelog][changelog-url]  
[changelog-url]: https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/blob/master/Changelog.md
See the [Changelog][changelog-url] to see the change history.

[//]: # (Labels)

[downloads-image]: https://img.shields.io/nuget/dt/Valtech.Amsterdam.Labs.Linq.SorterExtensions.svg
[nuget-url]: https://www.nuget.org/packages/Valtech.Amsterdam.Labs.Linq.SorterExtensions
[build-url]: https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/pipelines
[build-image]: https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/badges/master/build.svg
[coveralls-url]: https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/commits/master
[coveralls-image]: https://gitlab.com/Valtech-Amsterdam/Linq.SorterExtensions/badges/master/coverage.svg?job=test